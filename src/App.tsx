import React from "react";
import { HashRouter as Router, Routes, Route } from "react-router-dom";
import { ChatGpt } from "./component/ChatGpt";
import { Home } from "./component/Home";
import { TestApp } from "./component/testApp/TestApp";
import { GlobalStyle } from "./styles/globalStyle";

function App() {
  return (
    <Router>
      <GlobalStyle />
      <Routes>
        <Route path="/" element={<TestApp />}></Route>
        <Route path="/1" element={<ChatGpt />}></Route>
      </Routes>
    </Router>
  );
}

export default App;
