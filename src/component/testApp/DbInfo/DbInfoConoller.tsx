import styled from "styled-components";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { api } from "../../../api";

const TableWrap = styled.div`
  width: 30%;
`;
const SettingTable = styled.table`
  border-collapse: collapse;
  width: 100%;
  margin-bottom: 1rem;
  font-size: 0.9rem;

  th {
    background-color: #f5f5f5;
    text-align: left;
    padding: 15px;
    text-align: center;
    font-size: 18px;
    font-weight: 600;
    border: 1px solid #ddd;
  }
  td {
    width: 50%;
    border: 1px solid #ddd;
    padding: 10px;
    text-align: center;
  }
  input {
    all: unset;
    border: 1px solid black;
    width: 50%;
    height: 100%;
    text-align: center;
  }
  button {
    all: unset;
    width: 50%;
    height: 35px;
    border: 1px solid #ddd;
    background-color: white;
    font-size: 16px;
    font-weight: 500;
  }
`;

export const DbInfoController = ({
  data,
  deleteDbData,
  getDBinfo,
  setSync,
}: any) => {
  const [sensorCount, setSensorCount] = useState(0);
  const [loading, setLoading] = useState(false);
  const {
    register,
    handleSubmit,
    getValues,
    formState: { errors, isValid },
  } = useForm({
    mode: "onChange",
  });

  const createDbData = async (router: number, ratio: number) => {
    await api.get(`create/${data}/${router}/${ratio}`);
    await getDBinfo();
  };

  const onSubmit = async () => {
    if (loading) {
      return;
    }
    setLoading(true);
    const router = Number(getValues(`router`));
    const ratio = Number(getValues(`ratio`));
    if (data === "A") {
      setSync(false);
    }
    await deleteDbData(data);
    await createDbData(router, ratio);
    setLoading(false);
  };
  const valueChange = () => {
    const router = getValues(`router`);
    const ratio = getValues(`ratio`);
    setSensorCount(router * ratio);
    if (typeof router === "number") {
    }
  };

  return (
    <TableWrap key={data}>
      <form
        onSubmit={handleSubmit(() => {
          onSubmit();
        })}
      >
        <SettingTable>
          <thead>
            <tr>
              <th colSpan={2}>Type {data}</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>중계기 수</td>
              <td>
                <input
                  autoCapitalize="off"
                  style={{ borderColor: `${isValid ? "" : "red"}` }}
                  defaultValue={0}
                  {...register(`router`, {
                    required: "값은 필수 입니다.",
                    pattern: {
                      value: /^[0-9]+$/,
                      message: "숫자만 입력해주세요",
                    },
                    maxLength: {
                      value: 3,
                      message: "999까지만 입력가능합니다.",
                    },
                    onChange() {
                      valueChange();
                    },
                  })}
                ></input>
              </td>
            </tr>
            <tr>
              <td>센서 수</td>
              <td>{sensorCount}</td>
            </tr>
            <tr>
              <td>형태 비율</td>
              <td>
                1 :
                <input
                  autoCapitalize="off"
                  defaultValue={0}
                  {...register(`ratio`, {
                    required: "값은 필수 입니다.",
                    pattern: {
                      value: /^[0-9]+$/,
                      message: "숫자만 입력해주세요",
                    },
                    maxLength: {
                      value: 2,
                      message: "99까지만 입력가능합니다.",
                    },
                    onChange() {
                      valueChange();
                    },
                  })}
                  style={{
                    width: "30px",
                    marginLeft: "5px",
                    borderColor: `${isValid ? "" : "red"}`,
                  }}
                ></input>
              </td>
            </tr>
            <tr>
              <td
                colSpan={2}
                style={{ backgroundColor: "#f5f5f5", padding: "5px" }}
              >
                <button
                  style={{
                    backgroundColor: `${
                      isValid ? `${loading ? "" : "#2618b1"}` : ""
                    }`,
                    color: `${isValid ? `${loading ? "" : "white"}` : ""}`,
                    cursor: `${isValid ? `${loading ? "" : "pointer"}` : ""}`,
                  }}
                >
                  {loading ? "수정중..." : "수정하기"}
                </button>
              </td>
            </tr>
            {errors?.router?.message && (
              <div style={{ color: "red", marginTop: "10px" }}>
                값을 확인해주세요
              </div>
            )}
          </tbody>
        </SettingTable>
      </form>
    </TableWrap>
  );
};
