import { useForm } from "react-hook-form";
import styled from "styled-components";
import { useState, useEffect } from "react";
import { DbInfoController } from "./DbInfoConoller";
import { api } from "../../../api";

const Container = styled.div`
  width: 100%;
  max-width: 1320px;
  margin: 0 auto;

  margin-bottom: 100px;
`;
const Title = styled.div`
  font-size: 24px;
  margin-bottom: 30px;
  font-weight: 600;
`;
const ConWrap = styled.div`
  padding: 20px;
  border: 1px solid #ddd;
`;
const DbSettingWrap = styled.div`
  margin-bottom: 50px;
  &:last-child {
    margin-bottom: 0;
  }
`;
const SubTitle = styled.div`
  font-size: 20px;
  margin-bottom: 10px;
`;

const StyledTable = styled.table`
  border-collapse: collapse;
  width: 100%;
  margin-bottom: 1rem;
  font-size: 0.9rem;

  th {
    background-color: #f5f5f5;
    text-align: left;
    padding: 0.5rem;
    text-align: center;
    border: 1px solid #ddd;
  }
  td {
    border: 1px solid #ddd;
    padding: 10px;
    text-align: center;
    &:nth-child(5) {
      width: 180px;
    }
    &:nth-child(6) {
      width: 180px;
    }
  }
`;
const ResetBtn = styled.div`
  width: 100px;
  height: 32px;
  background-color: #de4343;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  color: #fff;
  cursor: pointer;
`;
const DbBtn = styled.div`
  width: 100px;
  height: 32px;
  background-color: #2618b1;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  color: #fff;
  cursor: pointer;
  position: relative;
`;

const DataInfo = styled.div`
  width: 180px;
  height: 200px;
  background-color: #1d1d1d;
  position: absolute;
  bottom: -200px;
  left: 0;
  z-index: 9999;
  backdrop-filter: blur(5px);
  color: #eee;
  line-height: 20px;
  text-align: left;
  padding: 10px;
`;
const SettingWrap = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const DbInfo = ({
  getDBinfo,
  routerCount,
  sensorCount,
  setSync,
}: any) => {
  const RouterType = ["A", "B", "C"];

  const [aTypeData, setATypeData] = useState(false);
  const [bTypeData, setBTypeData] = useState(false);
  const [cTypeData, setCTypeData] = useState(false);

  const deleteDbData = async (type: string) => {
    await api.get(`/delete/${type}`);
    await getDBinfo();
  };

  return (
    <Container>
      <Title>Test 환경설정</Title>
      <ConWrap>
        <DbSettingWrap>
          <SubTitle>DB 환경 정보</SubTitle>
          <StyledTable>
            <thead>
              <tr>
                <th>Type</th>
                <th>중계기 수</th>
                <th>센서 수</th>
                <th>형태</th>
                <th>초기화</th>
                <th>Data</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>A</td>
                <td>{routerCount[0]}</td>
                <td>{sensorCount[0]}</td>
                <td>
                  {sensorCount[0] === 0
                    ? 0
                    : `1 :
                  ${sensorCount[0] / routerCount[0]}`}
                </td>
                <td>
                  <ResetBtn
                    onClick={() => {
                      deleteDbData("A");
                      setSync(false);
                    }}
                  >
                    초기화
                  </ResetBtn>
                </td>
                <td>
                  <DbBtn
                    onClick={() => {
                      setATypeData(!aTypeData);
                      setBTypeData(false);
                      setCTypeData(false);
                    }}
                  >
                    보기
                    {aTypeData && (
                      <DataInfo>
                        <div>{`{`}</div>
                        <div>{`"data" :`}</div>
                        <div>{`{ "data" : 데이터 , "sensorId" : 센서 UUID, "sensingDate" :데이터 수집 시간}",`}</div>
                        <div>{`"routerUuid" : 중계기 UUID`}</div>
                        <div>{`}`}</div>
                      </DataInfo>
                    )}
                  </DbBtn>
                </td>
              </tr>
              <tr>
                <td>B</td>
                <td>{routerCount[1]}</td>
                <td>{sensorCount[1]}</td>
                <td>
                  {sensorCount[1] === 0
                    ? 0
                    : `1 :
                  ${sensorCount[1] / routerCount[1]}`}
                </td>
                <td>
                  <ResetBtn
                    onClick={() => {
                      deleteDbData("B");
                    }}
                  >
                    초기화
                  </ResetBtn>
                </td>
                <td>
                  <DbBtn
                    onClick={() => {
                      setATypeData(false);
                      setBTypeData(!bTypeData);
                      setCTypeData(false);
                    }}
                  >
                    보기
                    {bTypeData && (
                      <DataInfo>
                        <div>{`{`}</div>
                        <div>{`"data" :`}</div>
                        <div>{`{ "data" : 데이터 , "sensorId" : 센서 UUID, "sensingDate" :데이터 수집 시간, "img" : 이미지, "imgExt": 이미지 확장자}",`}</div>
                        <div>{`"routerUuid" : 중계기 UUID`}</div>
                        <div>{`}`}</div>
                      </DataInfo>
                    )}
                  </DbBtn>
                </td>
              </tr>
              <tr>
                <td>C</td>
                <td>{routerCount[2]}</td>
                <td>{sensorCount[2]}</td>
                <td>
                  {sensorCount[2] === 0
                    ? 0
                    : `1 :
                  ${sensorCount[2] / routerCount[2]}`}
                </td>
                <td>
                  <ResetBtn
                    onClick={() => {
                      deleteDbData("C");
                    }}
                  >
                    초기화
                  </ResetBtn>
                </td>
                <td>
                  <DbBtn
                    onClick={() => {
                      setATypeData(false);
                      setBTypeData(false);
                      setCTypeData(!cTypeData);
                    }}
                  >
                    보기
                    {cTypeData && <DataInfo>none</DataInfo>}
                  </DbBtn>
                </td>
              </tr>
            </tbody>
          </StyledTable>
        </DbSettingWrap>
        <DbSettingWrap>
          <SubTitle>DB 환경 변경</SubTitle>
          <SettingWrap>
            {RouterType.map((data, index) => (
              <DbInfoController
                key={index}
                data={data}
                deleteDbData={deleteDbData}
                getDBinfo={getDBinfo}
                setSync={setSync}
              />
            ))}
          </SettingWrap>
        </DbSettingWrap>
      </ConWrap>
    </Container>
  );
};
