import { useEffect, useState } from "react";
import styled from "styled-components";
import { api } from "../../../api";
import { useForm } from "react-hook-form";

const Container = styled.div`
  width: 30%;
`;
const Title = styled.div`
  font-size: 32px;
  font-weight: 900;
  margin: 20px 0;
  text-align: center;
`;
const BoxWrap = styled.div`
  border: 1px solid #dbdbdb;
  padding: 20px;
  font-size: 18px;
  form {
    width: 100%;
    display: flex;
    flex-direction: column;
    input {
      all: unset;
      box-sizing: border-box;
      border: 1px solid #1d1d1d;
      padding: 10px;
      margin-bottom: 15px;
      width: 100%;
    }
  }
`;

const SettingValueWrap = styled.div``;
const StyledTable = styled.table`
  border-collapse: collapse;
  width: 100%;
  margin: 10px 0;
  font-size: 14px;

  th {
    background-color: #f5f5f5;
    text-align: left;
    padding: 0.5rem;
    text-align: center;
    border: 1px solid #ddd;
  }
  td {
    border: 1px solid #ddd;
    padding: 10px;
    text-align: center;
    &:nth-child(5) {
      width: 180px;
    }
    &:nth-child(6) {
      width: 180px;
    }
  }
`;
const Line = styled.div`
  width: 100%;
  height: 1px;
  background-color: #dbdbdb;
  margin: 12px 0;
`;
const RunDataWrap = styled.div`
  margin-bottom: 12px;
  font-size: 22px;
  div {
    margin: 12px 0;
  }
  span {
    color: red;
    font-weight: 600;
  }
`;
const ValueTable = styled.table`
  border-collapse: collapse;
  width: 100%;
  margin: 10px 0;
  font-size: 20px;

  th {
    background-color: #f5f5f5;
    text-align: left;
    padding: 0.5rem;
    text-align: center;
    border: 1px solid #ddd;
  }
  td {
    border: 1px solid #ddd;
    padding: 10px;
    text-align: center;
    &:nth-child(5) {
      width: 180px;
    }
    &:nth-child(6) {
      width: 180px;
    }
  }
`;
export const TypeATestInfo = ({
  routerCount,
  sensorCount,
  aCycle,
  aTimer,
  aTypeSendCount,
  testData,
}: any) => {
  return (
    <Container>
      <Title>Type A</Title>
      <BoxWrap>
        <SettingValueWrap>
          <div>설정 값</div>
          <StyledTable>
            <thead>
              <tr>
                <th>중계기 수</th>
                <th>센서 수</th>
                <th>전송 주기</th>
                <th>구성 비</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{routerCount[0]}</td>
                <td>{sensorCount[0]}</td>
                <td>{aCycle}ms</td>
                <td>{`1 : ${sensorCount[0] / routerCount[0]}`}</td>
              </tr>
            </tbody>
          </StyledTable>
        </SettingValueWrap>
        <Line></Line>
        <RunDataWrap>
          <div>결과</div>
          <ValueTable>
            <thead>
              <tr>
                <th>구분</th>
                <th>값</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>시간</td>
                <td>{aTimer} s</td>
              </tr>
              <tr>
                <td>요청</td>
                <td>{aTypeSendCount * sensorCount[0]} 건</td>
              </tr>
              <tr>
                <td>데이터 처리</td>
                <td>{testData?.successData[0]} 건</td>
              </tr>
              <tr>
                <td>처리 대기</td>
                <td>
                  <span>
                    {aTypeSendCount * sensorCount[0] - testData?.successData[0]}{" "}
                    건
                  </span>
                </td>
              </tr>
              <tr>
                <td>평균</td>
                <td>{Math.floor(testData?.successData[0] / aTimer)} / s</td>
              </tr>
            </tbody>
          </ValueTable>
        </RunDataWrap>
        <Line></Line>
        <SettingValueWrap>
          <div>생성 데이터 평가</div>
          <StyledTable>
            <thead>
              <tr>
                <th
                  style={{
                    fontSize: "20px",
                    color: "#1b9e3e",
                    width: "33.3333%",
                  }}
                >
                  안전
                </th>
                <th
                  style={{
                    fontSize: "20px",
                    color: "#f9b115",
                    width: "33.3333%",
                  }}
                >
                  경고
                </th>
                <th
                  style={{
                    fontSize: "20px",
                    color: "#e55353",
                    width: "33.3333%",
                  }}
                >
                  위험
                </th>
              </tr>
            </thead>
            <tbody>
              <tr style={{ fontSize: "20px" }}>
                <td>
                  {testData?.successData[0] -
                    testData?.warnMinCount -
                    testData?.minCount}
                  건
                </td>
                <td>{testData?.warnMinCount}건</td>
                <td>{testData?.minCount}건</td>
              </tr>
            </tbody>
          </StyledTable>
        </SettingValueWrap>
      </BoxWrap>
    </Container>
  );
};
