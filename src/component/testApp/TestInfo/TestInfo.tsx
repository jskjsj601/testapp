import { useEffect, useState } from "react";
import styled from "styled-components";
import { TypeATestInfo } from "./TypeATestInfo";
import { TypeBTestInfo } from "./TypeBTestInfo";
import { TypeCTestInfo } from "./TypeCTestInfo";

const Container = styled.div`
  width: 100%;
  max-width: 1320px;
  margin: 0 auto;

  margin-bottom: 100px;
`;
const Title = styled.div`
  font-size: 24px;
  margin-bottom: 30px;
  font-weight: 600;
`;
const ConWrap = styled.div`
  padding: 20px;
  border: 1px solid #ddd;
`;
const DbSettingWrap = styled.div`
  display: flex;
  justify-content: space-between;
`;
const SubTitle = styled.div`
  font-size: 20px;
  margin-bottom: 10px;
`;
const Box = styled.div`
  width: 300px;
  height: 200px;
  background-color: red;
`;

export const TestInfo = ({
  routerCount,
  sensorCount,
  aCycle,
  aTimer,
  aTypeSendCount,
  bTypeSendCount,
  bTimer,
  bCycle,
  imgSendCount,
  testData,
}: any) => {
  return (
    <Container>
      <Title>Test 결과</Title>
      <ConWrap>
        <DbSettingWrap>
          <TypeATestInfo
            routerCount={routerCount}
            sensorCount={sensorCount}
            aCycle={aCycle}
            aTimer={aTimer}
            aTypeSendCount={aTypeSendCount}
            testData={testData}
          />
          <TypeBTestInfo
            routerCount={routerCount}
            sensorCount={sensorCount}
            bTypeSendCount={bTypeSendCount}
            bTimer={bTimer}
            bCycle={bCycle}
            imgSendCount={imgSendCount}
            testData={testData}
          />
          <TypeCTestInfo routerCount={routerCount} sensorCount={sensorCount} />
        </DbSettingWrap>
      </ConWrap>
    </Container>
  );
};
