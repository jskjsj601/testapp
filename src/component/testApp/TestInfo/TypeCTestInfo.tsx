import { useEffect, useState } from "react";
import styled from "styled-components";
import { api } from "../../../api";
import { useForm } from "react-hook-form";

const Container = styled.div`
  width: 30%;
`;
const Title = styled.div`
  font-size: 32px;
  font-weight: 900;
  margin: 20px 0;
  text-align: center;
`;
const BoxWrap = styled.div`
  border: 1px solid #dbdbdb;
  padding: 20px;
  font-size: 18px;
  form {
    width: 100%;
    display: flex;
    flex-direction: column;
    input {
      all: unset;
      box-sizing: border-box;
      border: 1px solid #1d1d1d;
      padding: 10px;
      margin-bottom: 15px;
      width: 100%;
    }
  }
`;

const SettingValueWrap = styled.div``;
const StyledTable = styled.table`
  border-collapse: collapse;
  width: 100%;
  margin: 10px 0;
  font-size: 14px;

  th {
    background-color: #f5f5f5;
    text-align: left;
    padding: 0.5rem;
    text-align: center;
    border: 1px solid #ddd;
  }
  td {
    border: 1px solid #ddd;
    padding: 10px;
    text-align: center;
    &:nth-child(5) {
      width: 180px;
    }
    &:nth-child(6) {
      width: 180px;
    }
  }
`;
const Line = styled.div`
  width: 100%;
  height: 1px;
  background-color: #dbdbdb;
  margin: 12px 0;
`;
const RunDataWrap = styled.div`
  margin-bottom: 12px;
  font-size: 22px;
  div {
    margin: 12px 0;
  }
  span {
    color: red;
    font-weight: 600;
  }
`;
const ValueTable = styled.table`
  border-collapse: collapse;
  width: 100%;
  margin: 10px 0;
  font-size: 20px;

  th {
    background-color: #f5f5f5;
    text-align: left;
    padding: 0.5rem;
    text-align: center;
    border: 1px solid #ddd;
  }
  td {
    border: 1px solid #ddd;
    padding: 10px;
    text-align: center;
    &:nth-child(5) {
      width: 180px;
    }
    &:nth-child(6) {
      width: 180px;
    }
  }
`;
export const TypeCTestInfo = ({ routerCount, sensorCount }: any) => {
  return (
    <Container>
      <Title>Type C</Title>
      <BoxWrap>
        <SettingValueWrap>
          <div>설정 값</div>
          <StyledTable>
            <thead>
              <tr>
                <th>중계기 수</th>
                <th>센서 수</th>
                <th>전송 주기</th>
                <th>구성 비</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>100</td>
                <td>2000</td>
                <td>6초</td>
                <td>1 : 20</td>
              </tr>
            </tbody>
          </StyledTable>
        </SettingValueWrap>
        <Line></Line>
        <RunDataWrap>
          <div>결과</div>
          <ValueTable>
            <thead>
              <tr>
                <th>구분</th>
                <th>값</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>시간</td>
                <td>{} s</td>
              </tr>
              <tr>
                <td>요청</td>
                <td>{} 건</td>
              </tr>
              <tr>
                <td>데이터 생성</td>
                <td>{} 건</td>
              </tr>
              <tr>
                <td>데이터 처리</td>
                <td>{} 건</td>
              </tr>
              <tr>
                <td>처리 대기</td>
                <td>
                  <span>{} 건</span>
                </td>
              </tr>
              <tr>
                <td>평균</td>
                <td>101 건 / s</td>
              </tr>
            </tbody>
          </ValueTable>
        </RunDataWrap>
        <Line></Line>
        <SettingValueWrap>
          <div>생성 데이터 평가</div>
          <StyledTable></StyledTable>
        </SettingValueWrap>
      </BoxWrap>
    </Container>
  );
};
