import { useEffect, useRef, useState } from "react";
import styled from "styled-components";
import { api } from "../../../api";
import { useForm } from "react-hook-form";

const Container = styled.div`
  width: 30%;
`;
const Title = styled.div`
  font-size: 32px;
  font-weight: 900;
  margin: 20px 0;
  text-align: center;
`;
const BoxWrap = styled.div`
  border: 1px solid #dbdbdb;
  padding: 20px;
  form {
    width: 100%;
    display: flex;
    flex-direction: column;
    input {
      all: unset;
      box-sizing: border-box;
      border: 1px solid #1d1d1d;
      padding: 10px;
      margin-bottom: 15px;
      width: 100%;
    }
  }
`;

const ConWrap = styled.div``;

const Group = styled.div``;
const GroupTitle = styled.div`
  font-size: 20px;
  font-weight: 400;
  margin-bottom: 10px;
`;
const InputWrap = styled.div`
  border: 1px solid #dbdbdb;
  padding: 20px;
  margin-bottom: 30px;
`;
const Label = styled.label`
  display: block;
  margin-bottom: 10px;
  span {
    color: red;
    font-size: 12px;
    margin-left: 3px;
  }
`;
const Input = styled.input`
  display: block;
`;
const Button = styled.button`
  all: unset;
  width: 100%;
  height: 50px;
  padding: 10px;
  text-align: center;
  box-sizing: border-box;
  color: white;
  transition: 0.5s;
`;

export const TypeAInfo = ({
  setATypeSendCount,
  setATimer,
  setACycle,
  sync,
  setSync,
}: any) => {
  const [start, setStart] = useState(false);
  const intervalRef = useRef<any>(null);
  const cycleRef = useRef<any>(null);
  const {
    register,
    formState: { errors, isValid },
    getValues,
  } = useForm({
    mode: "onChange",
  });
  // ==============================================================================
  const synchronization = (e: any) => {
    e.preventDefault();
    if (!isValid || start) {
      return;
    }
    if (sync) {
      setSync(false);
      return;
    }
    const { alertRate, riskRatio } = getValues();
    const danger = Math.floor(10000 / riskRatio + 1);
    const warning = Math.floor(danger + 10000 / alertRate);
    api.get(`/update/A/${danger}/${warning}`).then((res) => {
      if (res.data) {
        setSync(true);
      }
    });
  };
  // ============================================================================동기화

  // ==============================================================================
  const startTimer = (timer: number) => {
    cycleRef.current = setInterval(async () => {
      api.get("/start/A/a.png");
      setATypeSendCount((aTypeSendCount: number) => aTypeSendCount + 1);
    }, timer);
    intervalRef.current = setInterval(() => {
      setATimer((atimer: number) => atimer + 1);
    }, 1000);
    setStart(true);
  };

  const stopTimer = () => {
    clearInterval(cycleRef.current);
    clearInterval(intervalRef.current);
    setStart(false);
  };

  const startClick = async (e: any) => {
    e.preventDefault();

    if (!sync) {
      return;
    }

    if (!start) {
      const { timer } = getValues();
      await api.get("/startDate/A");
      setATypeSendCount(0);
      setATimer(0);
      startTimer(timer);
      setACycle(timer);
    } else {
      stopTimer();
    }
  };
  // ==================================================================시작 버튼
  return (
    <Container>
      <Title>Type A</Title>
      <BoxWrap>
        <ConWrap>
          <form>
            <Group>
              <GroupTitle>API환경 컨트롤러</GroupTitle>
              <InputWrap>
                <Label>
                  중계기 전송 주기 <span>(1000 ~) ms</span>
                </Label>
                <Input
                  disabled={sync}
                  defaultValue={1000}
                  style={{
                    backgroundColor: `${sync ? "rgba(0,0,0,0.3)" : ""}`,
                  }}
                  {...register("timer", {
                    pattern: {
                      value: /^[0-9]+$/,
                      message: "숫자만 입력해주세요",
                    },
                  })}
                />
              </InputWrap>
            </Group>
            <Group>
              <GroupTitle>Data Controller</GroupTitle>
              <InputWrap>
                <Label>경고 발생 비율</Label>
                <Input
                  defaultValue={5000}
                  disabled={sync}
                  style={{
                    width: "50%",
                    backgroundColor: `${sync ? "rgba(0,0,0,0.3)" : ""}`,
                  }}
                  {...register("alertRate", {
                    pattern: {
                      value: /^[0-9]+$/,
                      message: "숫자만 입력해주세요",
                    },
                  })}
                />
                <span style={{ marginLeft: "10px", fontSize: "20px" }}>
                  : 1
                </span>
                <Label>위험 발생 비율</Label>
                <Input
                  disabled={sync}
                  defaultValue={10000}
                  style={{
                    width: "50%",
                    backgroundColor: `${sync ? "rgba(0,0,0,0.3)" : ""}`,
                  }}
                  {...register("riskRatio", {
                    pattern: {
                      value: /^[0-9]+$/,
                      message: "숫자만 입력해주세요",
                    },
                  })}
                />
                <span style={{ marginLeft: "10px", fontSize: "20px" }}>
                  : 1
                </span>
              </InputWrap>
            </Group>
            <Button
              style={{
                backgroundColor: `${
                  sync
                    ? "rgba(0,0,0,0.3)"
                    : isValid
                    ? "#de4343"
                    : "rgba(0,0,0,0.3)"
                }`,

                marginBottom: "20px",
                cursor: "pointer",
              }}
              onClick={(e) => {
                synchronization(e);
              }}
            >
              {sync ? "재설정" : "동기화"}
            </Button>
            <Button
              style={{
                backgroundColor: `${
                  sync ? `${!start ? "#2618b1" : "#de4343"}` : "rgba(0,0,0,0.3)"
                }`,
                cursor: `${sync ? "pointer" : ""}`,
              }}
              onClick={(e) => {
                startClick(e);
              }}
            >
              {!start ? "시작" : "정지"}
            </Button>
          </form>
        </ConWrap>
      </BoxWrap>
    </Container>
  );
};
