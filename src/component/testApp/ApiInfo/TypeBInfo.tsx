import { useEffect, useState, useRef } from "react";
import styled from "styled-components";
import { api } from "../../../api";
import { useForm } from "react-hook-form";

const Container = styled.div`
  width: 30%;
  height: 100%;
`;
const Title = styled.div`
  font-size: 32px;
  font-weight: 900;
  margin: 20px 0;
  text-align: center;
`;
const BoxWrap = styled.div`
  border: 1px solid #dbdbdb;
  padding: 20px;
  form {
    width: 100%;
    display: flex;
    flex-direction: column;
    input {
      all: unset;
      box-sizing: border-box;
      border: 1px solid #1d1d1d;
      padding: 10px;
      margin-bottom: 15px;
      width: 100%;
    }
  }
`;

const ConWrap = styled.div``;

const Group = styled.div``;
const GroupTitle = styled.div`
  font-size: 20px;
  font-weight: 400;
  margin-bottom: 10px;
`;
const InputWrap = styled.div`
  border: 1px solid #dbdbdb;
  padding: 20px;
  margin-bottom: 30px;
`;
const Label = styled.label`
  display: block;
  margin-bottom: 10px;
  span {
    color: red;
    font-size: 12px;
    margin-left: 3px;
  }
`;
const Input = styled.input`
  display: block;
`;

const Select = styled.select`
  box-sizing: border-box;
  border: 1px solid #1d1d1d;
  padding: 10px;
  margin-bottom: 15px;
  width: 100%;
  height: 45px;
`;
const Option = styled.option`
  padding: 10px;
  margin-bottom: 15px;
  width: 100%;
`;
const Button = styled.button`
  all: unset;
  width: 100%;
  height: 50px;
  padding: 10px;
  text-align: center;
  box-sizing: border-box;
  color: white;
  transition: 0.5s;
`;

export const TypeBInfo = ({
  setBTypeSendCount,
  setBTimer,
  setBCycle,
  setImgSendCount,
}: any) => {
  const [start, setStart] = useState(false);
  const intervalRef = useRef<any>(null);
  const cycleRef = useRef<any>(null);
  const {
    register,
    formState: { errors, isValid },
    getValues,
  } = useForm({
    mode: "onChange",
  });
  let imgRatio = 0;
  let imgUrl = "a.png";
  // ==============================================================================
  const startTimer = (timer: number, ratio: number, img: string) => {
    cycleRef.current = setInterval(async () => {
      imgRatio = imgRatio + 1;
      if (imgRatio % ratio === 0) {
        imgUrl = img;
        setImgSendCount((imgSendCount: number) => imgSendCount + 1);
      } else {
        imgUrl = "a.png";
      }
      await api.get(`/start/B/${imgUrl}`);
      setBTypeSendCount((aTypeSendCount: number) => aTypeSendCount + 1);
    }, timer);
    intervalRef.current = setInterval(() => {
      setBTimer((btimer: number) => btimer + 1);
    }, 1000);
    setStart(true);
  };

  const stopTimer = () => {
    clearInterval(cycleRef.current);
    clearInterval(intervalRef.current);
    imgRatio = 0;
    setStart(false);
  };

  const startClick = async (e: any) => {
    e.preventDefault();
    if (!start) {
      await api.get("/startDate/B");
      const { timer, ratio, img } = getValues();
      setImgSendCount(0);
      setBTypeSendCount(0);
      setBTimer(0);
      startTimer(timer, ratio, img);
      setBCycle(timer);
    } else {
      stopTimer();
    }
  };
  // ==================================================================시작 버튼

  return (
    <Container>
      <Title>Type B</Title>
      <BoxWrap>
        <ConWrap>
          <form>
            <Group>
              <GroupTitle>API환경 컨트롤러</GroupTitle>
              <InputWrap>
                <Label>
                  중계기 전송 주기 <span>(1000 ~) ms</span>
                </Label>
                <Input
                  disabled={start}
                  defaultValue={1000}
                  style={{
                    backgroundColor: `${start ? "rgba(0,0,0,0.3)" : ""}`,
                  }}
                  {...register("timer", {
                    pattern: {
                      value: /^[0-9]+$/,
                      message: "숫자만 입력해주세요",
                    },
                  })}
                />
              </InputWrap>
            </Group>
            <Group>
              <GroupTitle>Data 컨트롤러</GroupTitle>
              <InputWrap>
                <Label>Img파일 생성 비율</Label>
                <Input
                  disabled={start}
                  defaultValue={10}
                  style={{
                    backgroundColor: `${start ? "rgba(0,0,0,0.3)" : ""}`,
                    width: "50%",
                  }}
                  {...register("ratio", {
                    pattern: {
                      value: /^[0-9]+$/,
                      message: "숫자만 입력해주세요",
                    },
                  })}
                />
                <span style={{ marginLeft: "10px", fontSize: "20px" }}>
                  : 1
                </span>
                <Label>Img파일 해상도</Label>
                <Select
                  disabled={start}
                  style={{
                    backgroundColor: `${start ? "rgba(0,0,0,0.3)" : ""}`,
                    width: "50%",
                  }}
                  {...register("img")}
                >
                  <Option value={"1.png"} label={"80 * 60 (하)"}></Option>
                  <Option value={"2.png"} label={"120 * 80 (중)"}></Option>
                  <Option value={"3.png"} label={"160 * 100 (상)"}></Option>
                </Select>
              </InputWrap>
            </Group>

            <Button
              style={{
                backgroundColor: `${!start ? "#2618b1" : "#de4343"}`,
                cursor: "pointer",
              }}
              onClick={(e) => {
                startClick(e);
              }}
            >
              {!start ? "시작" : "정지"}
            </Button>
          </form>
        </ConWrap>
      </BoxWrap>
    </Container>
  );
};
