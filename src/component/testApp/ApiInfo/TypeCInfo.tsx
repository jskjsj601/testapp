import { useEffect, useState } from "react";
import styled from "styled-components";
import { api } from "../../../api";
import { useForm } from "react-hook-form";

const Container = styled.div`
  width: 30%;
`;
const Title = styled.div`
  font-size: 32px;
  font-weight: 900;
  margin: 20px 0;
  text-align: center;
`;
const BoxWrap = styled.div`
  border: 1px solid #dbdbdb;
  padding: 20px;
  form {
    width: 100%;
    display: flex;
    flex-direction: column;
    input {
      all: unset;
      box-sizing: border-box;
      border: 1px solid #1d1d1d;
      padding: 10px;
      margin-bottom: 15px;
      width: 100%;
    }
  }
`;

const ConWrap = styled.div``;

const Group = styled.div``;
const GroupTitle = styled.div`
  font-size: 20px;
  font-weight: 400;
  margin-bottom: 10px;
`;
const InputWrap = styled.div`
  border: 1px solid #dbdbdb;
  padding: 20px;
  margin-bottom: 30px;
`;
const Label = styled.label`
  display: block;
  margin-bottom: 10px;
  span {
    color: red;
    font-size: 12px;
    margin-left: 3px;
  }
`;
const Input = styled.input`
  display: block;
`;
const Button = styled.button`
  all: unset;
  width: 100%;
  height: 50px;
  padding: 10px;
  text-align: center;
  box-sizing: border-box;
  color: white;
  transition: 0.5s;
`;

export const TypeCInfo = ({ setCTypeSendCount, setCTimer, setCCycle }: any) => {
  const { register, handleSubmit, getValues } = useForm({
    mode: "onChange",
  });

  const onSubmit = () => {
    const value = getValues();
    console.log(value);
  };

  return (
    <Container>
      <Title>Type C</Title>
      <BoxWrap>
        <ConWrap>
          <form onSubmit={handleSubmit(onSubmit)}>
            <Group>
              <GroupTitle>API환경 컨트롤러</GroupTitle>
              <InputWrap>
                <Label>
                  중계기 전송 주기 <span>(1000 ~) ms</span>
                </Label>
                <Input />
              </InputWrap>
            </Group>
            <Group>
              <GroupTitle>Data 컨트롤러</GroupTitle>
              <InputWrap></InputWrap>
            </Group>

            <Button style={{ backgroundColor: "#2618b1" }}>시작</Button>
          </form>
        </ConWrap>
      </BoxWrap>
    </Container>
  );
};
