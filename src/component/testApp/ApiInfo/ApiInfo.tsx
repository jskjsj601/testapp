import styled from "styled-components";
import { ChatGpt } from "../../ChatGpt";
import { TypeAInfo } from "./TypeAInfo";
import { TypeBInfo } from "./TypeBInfo";
import { TypeCInfo } from "./TypeCInfo";

const Container = styled.div`
  width: 100%;
  max-width: 1320px;
  margin: 0 auto;

  margin-bottom: 100px;
`;
const Title = styled.div`
  font-size: 24px;
  margin-bottom: 30px;
  font-weight: 600;
`;
const ConWrap = styled.div`
  padding: 20px;
  border: 1px solid #ddd;
`;
const DbSettingWrap = styled.div`
  display: flex;
  justify-content: space-between;
`;
const SubTitle = styled.div`
  font-size: 20px;
  margin-bottom: 10px;
`;

export const ApiInfo = ({
  setATypeSendCount,
  setBTypeSendCount,
  setCTypeSendCount,
  setATimer,
  setBTimer,
  setCTimer,
  setACycle,
  setBCycle,
  setCCycle,
  setImgSendCount,
  sync,
  setSync,
}: any) => {
  return (
    <Container>
      <Title>API 환경설정</Title>
      <ConWrap>
        <DbSettingWrap>
          <TypeAInfo
            setATypeSendCount={setATypeSendCount}
            setATimer={setATimer}
            setACycle={setACycle}
            sync={sync}
            setSync={setSync}
          />
          <TypeBInfo
            setBTypeSendCount={setBTypeSendCount}
            setBTimer={setBTimer}
            setBCycle={setBCycle}
            setImgSendCount={setImgSendCount}
          />
          <TypeCInfo
            setCTypeSendCount={setCTypeSendCount}
            setCTimer={setCTimer}
            setCCycle={setCCycle}
          />
        </DbSettingWrap>
      </ConWrap>
    </Container>
  );
};
