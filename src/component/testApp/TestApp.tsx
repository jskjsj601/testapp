import styled from "styled-components";
import { api } from "../../api";
import { ApiInfo } from "./ApiInfo/ApiInfo";
import { DbInfo } from "./DbInfo/DbInfo";
import { TestInfo } from "./TestInfo/TestInfo";
import { useState, useEffect } from "react";

const Section = styled.section`
  padding-top: 120px;
`;
const Title = styled.div`
  text-align: center;
  font-size: 32px;
  font-weight: 600;
  margin-bottom: 60px;
`;
export const TestApp = () => {
  const [routerCount, setRouterCount] = useState([]);
  const [sensorCount, setSensorCount] = useState([]);
  const [testData, setTestData] = useState({});
  const [aTypeSendCount, setATypeSendCount] = useState(0);
  const [bTypeSendCount, setBTypeSendCount] = useState(0);
  const [cTypeSendCount, setCTypeSendCount] = useState(0);
  const [aTimer, setATimer] = useState(0);
  const [bTimer, setBTimer] = useState(0);
  const [cTimer, setCTimer] = useState(0);
  const [aCycle, setACycle] = useState(0);
  const [bCycle, setBCycle] = useState(0);
  const [cCycle, setCCycle] = useState(0);
  const [loading, setLoading] = useState<boolean>(false);
  const [imgSendCount, setImgSendCount] = useState(0);
  const [sync, setSync] = useState(false);

  const getDBinfo = async () => {
    const {
      data: { routerCount, sensorCount },
    } = await api.get("/select");
    setRouterCount(routerCount);
    setSensorCount(sensorCount);
  };

  const getTestInfo = async () => {
    const { data } = await api.get("/dataCheck");
    setTestData(data);
  };
  useEffect(() => {
    const didmount = async () => {
      await getDBinfo();
      await getTestInfo();
      const interval = await setInterval(() => {
        getTestInfo();
      }, 500);
      setLoading(true);
    };
    didmount();
  }, []);
  return (
    <Section>
      {loading ? (
        <>
          <Title>수소배관망 모니터링 TEST APP</Title>
          <DbInfo
            getDBinfo={getDBinfo}
            routerCount={routerCount}
            sensorCount={sensorCount}
            setSync={setSync}
          />
          <ApiInfo
            setATypeSendCount={setATypeSendCount}
            setBTypeSendCount={setBTypeSendCount}
            setATimer={setATimer}
            setBTimer={setBTimer}
            setCTimer={setCTimer}
            setACycle={setACycle}
            setBCycle={setBCycle}
            setCCycle={setCCycle}
            setImgSendCount={setImgSendCount}
            sync={sync}
            setSync={setSync}
          />
          <TestInfo
            routerCount={routerCount}
            sensorCount={sensorCount}
            aCycle={aCycle}
            aTimer={aTimer}
            aTypeSendCount={aTypeSendCount}
            bTypeSendCount={bTypeSendCount}
            bTimer={bTimer}
            bCycle={bCycle}
            imgSendCount={imgSendCount}
            testData={testData}
          />
        </>
      ) : (
        ""
      )}
    </Section>
  );
};
