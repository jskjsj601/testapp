import { useEffect, useState } from "react";
import styled from "styled-components";
import { api } from "../api";

const Section = styled.section`
  width: 100%;
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const BoxWrap = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 700px;
  width: 100%;
  border: 1px solid #dbdbdb;
  align-items: center;
  padding: 80px 50px;
  border-radius: 10px;
  form {
    width: 100%;
    display: flex;
    flex-direction: column;
    input {
      all: unset;
      border: 1px solid #1d1d1d;
      padding: 10px;
      margin-bottom: 15px;
      border-radius: 10px;
    }
  }
`;

const Title = styled.div`
  font-size: 42px;
  font-weight: 900;
  margin-bottom: 50px;
  text-align: center;
`;

const CreateWrap = styled.div`
  width: 100%;
  display: flex;
  justify-content: right;
  margin-bottom: 30px;
`;

const CreateButton = styled.div`
  padding: 15px 50px;
  background-color: red;
  border-radius: 10px;
  color: white;
`;
const ConWrap = styled.div``;

const Wrap = styled.div`
  display: flex;
`;

const Group = styled.div`
  margin-right: 30px;
  &:last-child {
    margin-right: 0;
  }
`;
const GroupTitle = styled.div`
  font-size: 22px;
  font-weight: 400;
  margin-bottom: 20px;
`;
const InputWrap = styled.div`
  border: 1px solid #dbdbdb;
  padding: 20px;
  border-radius: 10px;
  margin-bottom: 30px;
`;
const Label = styled.label`
  display: block;
  margin-bottom: 10px;
  span {
    color: red;
    font-size: 12px;
    margin-left: 3px;
  }
`;
const Input = styled.input`
  display: block;
`;
const Button = styled.button`
  all: unset;
  width: 100%;
  height: 50px;
  padding: 10px;
  text-align: center;
  box-sizing: border-box;
  color: white;
  border-radius: 10px;
  transition: 0.5s;
`;

export const Home = () => {
  const [sensorCount, setSensorCount] = useState(1);
  const [pushSensorCount, setPushSensorCount] = useState(1);
  const [routerCount, setRouterCount] = useState(1);
  const [time, setTime] = useState(1000);
  const [loading, setLoading] = useState(false);
  const [serverPlaying, setServerPlaying] = useState(false);
  const [serverGetData, setServerGetData] = useState(false);

  const checkSever = () => {
    api.get("/check").then((res) => {
      if (res.data.create) {
        setServerGetData(true);
      } else {
        setServerGetData(false);
      }

      if (res.data.run) {
        setServerPlaying(true);
      } else {
        setServerPlaying(false);
      }
      console.log(res);
    });
  };

  const submit = (e: any) => {
    e.preventDefault();

    if (loading) {
      alert("로딩 중 입니다.");

      return;
    }
    if (!serverGetData) {
      alert("UUID를 먼저 생성해주세요");
      return;
    }

    if (!sensorCount || !pushSensorCount || !routerCount || !time) {
      alert("설정 값을 확인해주세요");
      return;
    }
    setLoading(true);
    if (!serverPlaying) {
      api
        .get(
          `/setting/${time}/${sensorCount}/${pushSensorCount}/${routerCount}`
        )
        .then((res) => {
          api.get("/start");
          setTimeout(() => {
            checkSever();
            setLoading(false);
          }, 5000);
        })
        .catch((err) => {
          alert("서버와 연결되지 않습니다.");
          setLoading(false);
        });
    } else {
      api.get("/stop").then((res) => {
        setTimeout(() => {
          checkSever();
          setLoading(false);
        }, 5000);
      });
    }
  };

  const createUuid = () => {
    setLoading(true);
    if (serverGetData || loading) {
      setLoading(false);
    } else {
      api
        .get("/create")
        .then((res) => {
          setLoading(false);
          checkSever();
        })
        .catch((err) => {
          alert("서버와 연결되지 않습니다.");
          setLoading(false);
          console.log(err);
        });
    }
  };

  useEffect(() => {
    checkSever();
  }, []);

  return (
    <Section>
      <BoxWrap>
        <Title>
          수소모니터링
          <br />
          Test App
        </Title>
        <CreateWrap>
          <CreateButton
            style={{
              opacity: `${serverGetData || loading ? "0.5" : "1"}`,
              cursor: `${serverGetData ? "auto" : "pointer"}`,
            }}
            onClick={createUuid}
          >
            {loading ? "로딩 중" : "UUID생성"}
          </CreateButton>
        </CreateWrap>
        <ConWrap>
          <form>
            <Wrap>
              <Group>
                <GroupTitle>센서 컨트롤러</GroupTitle>
                <InputWrap>
                  <Label>
                    센서 수<span>(1 ~ 20)</span>
                  </Label>
                  <Input
                    style={{
                      backgroundColor: `${
                        serverPlaying || loading ? "rgba(0,0,0,0.15)" : ""
                      }`,
                    }}
                    disabled={serverPlaying}
                    value={sensorCount}
                    onChange={(e) => {
                      setSensorCount(Number(e.currentTarget.value));
                    }}
                  />
                  <Label>
                    센서 데이터 전송 수 <span>(1 ~ 센서 수)</span>
                  </Label>
                  <Input
                    style={{
                      backgroundColor: `${
                        serverPlaying || loading ? "rgba(0,0,0,0.15)" : ""
                      }`,
                    }}
                    disabled={serverPlaying}
                    value={pushSensorCount}
                    onChange={(e) => {
                      setPushSensorCount(Number(e.currentTarget.value));
                    }}
                  />
                </InputWrap>
              </Group>
              <Group>
                <GroupTitle>중계기 컨트롤러</GroupTitle>
                <InputWrap>
                  <Label>
                    중계기 수 <span>(1 ~ 304)</span>
                  </Label>
                  <Input
                    style={{
                      backgroundColor: `${
                        serverPlaying || loading ? "rgba(0,0,0,0.15)" : ""
                      }`,
                    }}
                    disabled={serverPlaying}
                    value={routerCount}
                    onChange={(e) => {
                      setRouterCount(Number(e.currentTarget.value));
                    }}
                  />
                  <Label>
                    중계기 전송 주기 <span>(1000 ~) ms</span>
                  </Label>
                  <Input
                    style={{
                      backgroundColor: `${
                        serverPlaying || loading ? "rgba(0,0,0,0.15)" : ""
                      }`,
                    }}
                    disabled={serverPlaying}
                    value={time}
                    onChange={(e) => {
                      setTime(Number(e.currentTarget.value));
                    }}
                  />
                </InputWrap>
              </Group>
            </Wrap>
            <Button
              onClick={(e) => {
                submit(e);
              }}
              style={{
                backgroundColor: `${serverPlaying ? "red" : "blueviolet"}`,
                opacity: `${loading ? "0.5" : "1"}`,
              }}
            >
              {loading ? "로딩 중" : `${serverPlaying ? "정지" : "시작"}`}
            </Button>
          </form>
        </ConWrap>
      </BoxWrap>
    </Section>
  );
};
