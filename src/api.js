import axios from "axios";

const LOGIN_STATUS_CODE = {
  SUCCESSFUL: 200, // 성공
  UNAUTHORIZED: 401, // 인증실패
  USER_NOT_USED: 406, // 유저 사용 불가
  USER_IS_LOCK: 402, // 유저 잠금 상태
  USER_IS_NOT_ALLOWS: 405, // 유저 사용 허용 불가
  ERROR_IS_DB_FAILED: 501, // DB 등록,수정,삭제 중 에러
  ERROR_IS_SERVER_ERROR: 500, // 서버에러
  ERROR_IS_INPUT_PARAMS_INVALID: 503, // 입력 파라메터 오류
};

export const api = axios.create({
  baseURL: "http://localhost:6113",
  withCredentials: true,
});
